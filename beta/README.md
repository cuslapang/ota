Thank you for your volunteering as a beta tester!

As you may know, before deploying each release on iodé phones, our team spends a few days testing that the release functions as expected. We are thus looking for beta tester volunteers.

There is a different version of the updater for each Android release, corresponding to iode major version number:
- UpdaterBetaIode1Vx.apk : to test beta updates of iodé 1.x (Android 10) and switch to iodé 2.x (Android 11) when available
- UpdaterBetaIode2Vx.apk : to test beta updates of iodé 2.x (Android 11) and switch to iodé 3.x (Android 12) when available
- UpdaterBetaIode3Vx.apk : to test beta updates of iodé 3.x (Android 12)

To uninstall a previous version of the updater, if the new one cannot be installed, go to App info of the updater (long press on its icon), three dots menu top right , "Uninstall updates" ; or adb uninstall org.lineageos.updater.

You will receive a notification from the updater to download then install each of our beta's. 
Please report to our team possible anomalies on the use of your smartphone via our [Telegram app group](https://t.me/joinchat/Nj4irBYAM--abzWN-F1pKg) or  Matrix app group (#iodeOS:matrix.org), or open an issue on gitlab.

If you encounter nothing wrong, then just enjoy the update before everyone 🙂.
